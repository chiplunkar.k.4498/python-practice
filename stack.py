class Stack:
    stack = []
    def __init__(self):
        print("Stack created.")

    def push(self, item):
        self.stack.append(item)

    def display_contents(self):
        print(self.stack)

    def pop(self):
        self.stack.pop()

stk_obj = Stack()
stk_obj.push(10)
stk_obj.push(20)
stk_obj.display_contents()
stk_obj.pop()
stk_obj.display_contents()
